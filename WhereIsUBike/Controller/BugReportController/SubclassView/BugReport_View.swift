//
//  BugReport_View.swift
//  WhereIsUBike
//
//  Created by t19960804 on 2018/11/16.
//  Copyright © 2018 t19960804. All rights reserved.
//

import Foundation
import UIKit

class BugReport_View: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
